## Pré configuração para utilização do ambiente local

Para configurar o ambiente local, segue a configuração para servidor Apache:

- No arquivo `/etc/apache2/sites-enabled/000-default.conf`, inserir:
```
<VirtualHost *:8010>
	ServerAdmin webmaster@localhost
	DocumentRoot /var/www/html/thealma/public_html
	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```

- No arquivo `/etc/apache2/envvars`, inserir:
```
export APPLICATION_ENV=development
```

- No arquivo `/etc/apache2/ports.conf`, inserir:
```
Listen 8010
```

- No arquivo `/etc/apache2/apache2.conf`

```
alterar o AllowOverride de *None* para *All*
```


Agora pode ser acessado o projeto pelo endereço:

[localhost:8010](http://localhost:8010/) 