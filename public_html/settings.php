<?php 

class Settings
{
	private $setting;
	private $module;
	private $controller;
	private $action;

	public function __construct($param)
	{
		$this->setting = $param;
		$this->setModule($param['module']);
		$this->setController($param['controller']);
		$this->setAction($param['action']);
	}

	public function create()
	{
		$this->file_exist($this->generateJs(), 'js');
		$this->file_exist($this->generateCss(), 'css');
	}

	private function file_exist($files, $type)
	{
		foreach ($files as $file) {
			if (file_exists($file)) {
				switch ($type) {
					case 'js':
						echo '<script type="text/javascript" src="/'.$file.'"></script>';
						break;
					case 'css':
						echo '<link rel="stylesheet" type="text/css" href="/'.$file.'">';
						break;
				}
			}
		}
	}

	private function generateJs()
	{
		return  [
			'assets/js/'.$this->getModule().'/index.js',
			'assets/js/'.$this->getModule().'/'.$this->getController().'/index.js',
			'assets/js/'.$this->getModule().'/'.$this->getController().'/'.$this->getModule().'.js',
			'assets/js/'.$this->getModule().'/'.$this->getController().'/'.$this->getModule().'.'.$this->getController().'.js',
			'assets/js/'.$this->getModule().'/'.$this->getController().'/'.$this->getModule().'.'.$this->getController().'.'.$this->getAction().'.js',
		];

	}

	private function generateCss()
	{
		return  [
			'assets/css/'.$this->getModule().'/index.css',
			'assets/css/'.$this->getModule().'/'.$this->getController().'/index.css',
			'assets/css/'.$this->getModule().'/'.$this->getController().'/'.$this->getModule().'.css',
			'assets/css/'.$this->getModule().'/'.$this->getController().'/'.$this->getModule().'.'.$this->getController().'.css',
			'assets/css/'.$this->getModule().'/'.$this->getController().'/'.$this->getModule().'.'.$this->getController().'.'.$this->getAction().'.css',
		];

	}

	private function setModule($module)
	{
		$this->module = $module;
	}
	
	private function setController($controller)
	{
		$this->controller = $controller;
	}
	
	private function setAction($action)
	{
		$this->action = $action;
	}

	public function getModule()
	{
		return $this->module;
	}

	public function getController()
	{
		return $this->controller;
	}

	public function getAction()
	{
		return $this->action;
	}

}