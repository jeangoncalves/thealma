<?php 

/**
* Rotas do Site
*/

class Site
{
	private $routes = [];
	private $tmp;

	function __construct()
	{
        $this->index();
	}

	public function getRoute()
	{
		return $this->routes;
	}

    private function index()
    {
        $this->tmp = [
            'default' => [
                'route' => '/',
                'module' => 'site',
                'controller' => 'index',
                'action' => 'index',
            ],
            'produtos' => [
                'route' => '/produtos',
                'module' => 'site',
                'controller' => 'index',
                'action' => 'produtos',
            ],
            'contato' => [
                'route' => '/contato',
                'module' => 'site',
                'controller' => 'index',
                'action' => 'contato',
            ],
        ];
        $this->routes += $this->tmp;
    }
}