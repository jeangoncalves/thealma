<?php 

/**
* Rotas do App
*/

class App
{
	private $routes = [];
	private $tmp;

	function __construct()
	{
		$this->index();
		$this->projeto();
		$this->orcamento();
		$this->configuracao();
		$this->sobre();
		$this->auth();
	}

	public function getRoute()
	{
		return $this->routes;
	}

	private function index()
	{
		$this->tmp = [
            'indexIndex' => [
                'route' => '/app',
                'module' => 'application',
                'controller' => 'index',
                'action' => 'index',
            ],
		];
		$this->routes += $this->tmp;
	}

	private function projeto()
	{
		$this->tmp = [
            'projetoIndex' => [
                'route' => '/app/projeto',
                'module' => 'application',
                'controller' => 'projeto',
                'action' => 'index',
            ],
		];
		$this->routes += $this->tmp;
	}

	private function orcamento()
	{
		$this->tmp = [
            'orcamentoIndex' => [
                'route' => '/app/orcamento',
                'module' => 'application',
                'controller' => 'orcamento',
                'action' => 'index',
            ],
		];
		$this->routes += $this->tmp;
	}

	private function configuracao()
	{
		$this->tmp = [
            'configuracaoHorasTrabalhadas' => [
                'route' => '/app/configuracao/horastrabalhadas',
                'module' => 'application',
                'controller' => 'configuracao',
                'action' => 'horasTrabalhadas',
            ],
            'configuracaoIndex' => [
                'route' => '/app/configuracao',
                'module' => 'application',
                'controller' => 'configuracao',
                'action' => 'index',
            ],
            'configuracaoCustosFixos' => [
                'route' => '/app/configuracao/custosfixos',
                'module' => 'application',
                'controller' => 'configuracao',
                'action' => 'custosFixos',
            ],
            'configuracaoProlabore' => [
                'route' => '/app/configuracao/prolabore',
                'module' => 'application',
                'controller' => 'configuracao',
                'action' => 'prolabore',
            ],
		];
		$this->routes += $this->tmp;
	}

	private function sobre()
	{
		$this->tmp = [
            'sobreIndex' => [
                'route' => '/app/sobre',
                'module' => 'application',
                'controller' => 'sobre',
                'action' => 'index',
            ],
		];
		$this->routes += $this->tmp;
	}

	private function auth()
	{
		$this->tmp = [
            'authLogin' => [
                'route' => '/app/login',
                'module' => 'application',
                'controller' => 'auth',
                'action' => 'login',
            ],
            'authLogout' => [
                'route' => '/app/logout',
                'module' => 'application',
                'controller' => 'auth',
                'action' => 'logout',
            ],
		];
		$this->routes += $this->tmp;
	}
}