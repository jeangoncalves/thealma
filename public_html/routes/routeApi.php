<?php 

/**
* Rotas da API
*/

class Api
{
	private $routes = [];
	private $tmp;

	function __construct()
	{
		$this->auth();
		$this->orcamento();
		$this->configuracao();
	}

	public function getRoute()
	{
		return $this->routes;
	}

	private function auth()
	{
		$this->tmp = [
            '_authLogin' => [
                'route' => '/v1/login',
                'module' => 'api',
                'controller' => 'auth',
                'action' => 'login',
            ],
        ];
        $this->routes += $this->tmp;
	}

	private function orcamento()
	{
		$this->tmp = [
			'_orcamentoIndex' => [
                'route' => '/v1/orcamento',
                'module' => 'api',
                'controller' => 'orcamento',
                'action' => 'index',
            ],
            '_orcamentoGetId' => [
                'route' => '/v1/orcamento/:id',
                'module' => 'api',
                'controller' => 'orcamento',
                'action' => 'getId',
                [
                    ':id' => '\d+'
                ]
            ],
            '_orcamentoAdd' => [
                'route' => '/v1/orcamento/add',
                'module' => 'api',
                'controller' => 'orcamento',
                'action' => 'add',
            ],
            '_orcamentoEdit' => [
                'route' => '/v1/orcamento/edit/:id',
                'module' => 'api',
                'controller' => 'orcamento',
                'action' => 'edit',
                [
                    ':id' => '\d+'
                ]
            ],
            '_orcamentoDel' => [
                'route' => '/v1/orcamento/del/:id',
                'module' => 'api',
                'controller' => 'orcamento',
                'action' => 'del',
                [
                    ':id' => '\d+'
                ]
            ],
            '_orcamentoRemoveStep' => [
                'route' => '/v1/orcamento/removeStep',
                'module' => 'api',
                'controller' => 'orcamento',
                'action' => 'removeStep',
            ],
		];
        $this->routes += $this->tmp;
	}

	private function configuracao()
	{
		$this->tmp = [
            // WorkTime
            '_configGetWorkTime' => [
                'route' => '/v1/configuracao/getWorkTime',
                'module' => 'api',
                'controller' => 'configuracao',
                'action' => 'getWorkTime',
            ],
            '_configAddWorkTime' => [
                'route' => '/v1/configuracao/addWorkTime',
                'module' => 'api',
                'controller' => 'configuracao',
                'action' => 'addWorkTime',
            ],

            // Prolabore
            '_configGetProlabore' => [
                'route' => '/v1/configuracao/getProlabore',
                'module' => 'api',
                'controller' => 'configuracao',
                'action' => 'getProlabore',
            ],
            '_configAddProlabore' => [
                'route' => '/v1/configuracao/addProlabore',
                'module' => 'api',
                'controller' => 'configuracao',
                'action' => 'addProlabore',
            ],

            // Custo Fixo
            '_configGetFixedCost' => [
                'route' => '/v1/configuracao/getFixedCost',
                'module' => 'api',
                'controller' => 'configuracao',
                'action' => 'getFixedCost',
            ],
            '_configAddFixedCost' => [
                'route' => '/v1/configuracao/addFixedCost',
                'module' => 'api',
                'controller' => 'configuracao',
                'action' => 'addFixedCost',
            ],
            '_configRemoveFixedCost' => [
                'route' => '/v1/configuracao/removeFixedCost',
                'module' => 'api',
                'controller' => 'configuracao',
                'action' => 'removeFixedCost',
            ],

            // Geral
            '_configGetAllConfiguration' => [
                'route' => '/v1/configuracao/getAllConfiguration',
                'module' => 'api',
                'controller' => 'configuracao',
                'action' => 'getAllConfiguration',
            ],
		];
        $this->routes += $this->tmp;
	}
}