<?php

require_once '../vendor/autoload.php';
require_once 'routes.php';
require_once 'settings.php';

if (getenv('APPLICATION_ENV') == 'development') {
    ini_set('display_errors', 1);
    error_reporting(-1);
}

$ini = '../src/app/config/config.ini';

$layoutPerModule = [
    'site' => [
        'site',
    ],
    'app' => [
        'application',
    ],
];

$config = new \Piano\Config\Ini($ini);

$routes = new Routes();

$router = new \Piano\Router();
$router
    ->setRoutes($routes->getRoutes())
    ->enableSearchEngineFriendly(true);

$app = new \Piano\Application($config, $router);
$app->registerModulesLayout($layoutPerModule);

if ($app->getModuleName() === 'application') {
    if ($app->getSession('user-id') === null && $app->getActionName() !== 'login') {
        $app->redirect('/application/auth/login');
    }
}

$app->run();


$settings = new Settings($router->getMatchedRoute());
$settings->create();
