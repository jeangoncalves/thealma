<?php 

class RoutesSettings
{
	protected $arrRoute = array();
	protected $name;
	protected $route;
	protected $module 
	protected $controller 
	protected $action 

	function __construct()
	{
		$this->arrRoute = [
            'route' => [
                'route' => '',
                'module' => '',
                'controller' => '',
                'action' => '',
            ],
        ];
	}

	public function getRoute()
	{
		$this->arrRoute = [
            $this->name => [
                'route' => $this->route,
                'module' => $this->module,
                'controller' => $this->controller,
                'action' => $this->action
            ],
        ];
		return $this->arrRoute;
	}

	public function setName($name)
	{
		$this->name = $name;
	}

	public function setRoute($route)
	{
		$this->route = $route;
	}

	public function setModule($module)
	{
		$this->module = $module;
	}

	public function setController($controller)
	{
		$this->controller = $controller;
	}

	public function setAction($action)
	{
		$this->action = $action;
	}
}