<?php

include 'routes/routeApi.php';
include 'routes/routeApp.php';
include 'routes/routeSite.php';

class Routes
{
    private $routes = array();

    function __construct()
    {
        $api = new Api;
        $this->routes += $api->getRoute();

        $app = new App;
        $this->routes += $app->getRoute();

        $site = new Site;
        $this->routes += $site->getRoute();
    }

    public function getRoutes()
    {
        return $this->routes;
    }
}
