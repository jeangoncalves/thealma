var FixedCost = function() {

	var callback;

	var fixedCost = function() {
		var self = this;

		///////////////////////////////////////////////////////////
		// Variables
		//
		self.arrFixedCost = ko.observableArray();

		self.totaFixedCost = ko.computed(function() {
			var total = 0;
			$.each(self.arrFixedCost(), function(k, v) {
				total += +v.value();
			});
			return total;
		});

		///////////////////////////////////////////////////////////
		// Functions
		//
		self.init = function() {
			self.getValues();
			self.listener();
		};
		self.getValues = function() {
			self.arrFixedCost.removeAll();
			$.ajax({
				url: '/v1/configuracao/getFixedCost',
				type: 'GET',
				dataType: 'json',
				success: function(data) {
					$.each(data.response, function(k, v) {
						self.arrFixedCost.push(new cost(v));
					});
				}
			});
		};

		self.listener = function() {
			$('[fixed-cost-button-new]').on('click', function() {
				self.arrFixedCost.push(new cost);
			});
			$('[fixed-cost-button-save]').on('click', function() {
				$.ajax({
					url: '/v1/configuracao/addFixedCost',
					type: 'POST',
					dataType: 'json',
					data: {
						values: self.arrFixedCost()
					},
					success: function(data) {

					}
				});
			});
		};

		self.removeCost = function(cost) {
			$.ajax({
				url: '/v1/configuracao/removeFixedCost',
				type: 'POST',
				dataType: 'json',
				data: {
					id: cost.id()
				},
				success: function(data) {
					if (data.response) {
						self.arrFixedCost.remove(cost);
					}
				}
			});
		}
	};

	var cost = function(c) {
		var self = this;

		self.id = ko.observable((c !== undefined) ? c.id : '');
		self.name = ko.observable((c !== undefined) ? c.sName : '');
		self.type = ko.observable((c !== undefined) ? c.eType : '');
		self.value = ko.observable((c !== undefined) ? c.fValue : '');
	};

	var init = function() {
		window.fixedCost = new fixedCost;
		ko.applyBindings(window.fixedCost, document.getElementById('fixedCost'));
		window.fixedCost.init();
	};

	var callback = function(func) {
		callback = func;
	};

	return {
		init: init,
		callback: function(func) {
			if ((typeof func !== 'function')) throw Error('invalid argument: callback is not a function');
			callback(func);
		}
	}
};