var WorkTime = function() {

	var callback;

	var workTime = function() {
		var self = this;

		///////////////////////////////////////////////////////////
		// Variables
		//
		self.days = ko.observable(0);

		self.hours = ko.observable(0);

		self.daysMonth = ko.computed(function() {
			return Math.round(self.days() * 4.2);
		});

		self.hoursMonth = ko.computed(function() {
			return Math.round(self.daysMonth() * self.hours());
		});

		///////////////////////////////////////////////////////////
		// Functions
		//
		self.init = function() {
			self.getValues();
			self.listener();
		};

		self.getValues = function() {
			$.ajax({
				url: '/v1/configuracao/getWorkTime',
				type: 'GET',
				dataType: 'json',
				success: function(data) {
					self.days(data.response.iDays);
					self.hours(data.response.fHours);
				}
			});
		};

		self.listener = function() {
			$('[worktime-button-save]').on('click', function() {
				$.ajax({
					url: '/v1/configuracao/addWorkTime',
					type: 'POST',
					data: {
						days: self.days(),
						hours: self.hours(),
					},
					success: function(data) {
						$('.msg-success').show('slow');
						setTimeout(function() {
							$('.msg-success').hide('slow');
						}, 2000);
					}
				});
			});
		};
	};

	var init = function() {
		window.workTime = new workTime;
		ko.applyBindings(window.workTime, document.getElementById('workTime'));
		window.workTime.init();
	};

	var callback = function(func) {
		callback = func;
	};

	return {
		init: init,
		callback: function(func) {
			if ((typeof func !== 'function')) throw Error('invalid argument: callback is not a function');
			callback(func);
		}
	}
};