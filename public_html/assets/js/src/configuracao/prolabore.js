var Prolabore = function() {

	var callback;

	var prolabore = function() {
		var self = this;

		///////////////////////////////////////////////////////////
		// Variables
		//
		self.mark = ko.observable(0);
		
		self.costProduction = ko.computed(function() {
			var total = Number(fixedCost.totaFixedCost()) / Number(workTime.hoursMonth());
			if (isNaN(total)) {
				return 0;
			}
			return total.toFixed(2);
		});

		self.costProlabore = ko.computed(function() {
			var total = Number(self.mark()) / Number(workTime.hoursMonth());
			if (isNaN(total)) {
				return 0;
			}
			return total.toFixed(2);
		});

		self.totalHour = ko.computed(function() {
			var total = Number(self.costProlabore()) + Number(self.costProduction());
			if (isNaN(total)) {
				return 0;
			}
			return total.toFixed(2);
		});

		self.markMonth = ko.computed(function() {
			var total = Number(workTime.hoursMonth()) * Number(self.totalHour());
			if (isNaN(total)) {
				return 0;
			}
			return total.toFixed(2);
		});

		self.markYear = ko.computed(function() {
			var total = Number(self.markMonth()) * 12;
			return total.toFixed(2);
		});

		self.tributeCost = ko.computed(function() {
			if (self.markMonth() < Number(1903.99)) {
				return 0;
			} else if (self.markMonth() >= Number(1903.99) && self.markMonth() < Number(2826.65)) {
				return Number(7.5);
			} else if (self.markMonth() >= Number(2826.65) && self.markMonth() < Number(3751.06)) {
				return Number(15);
			} else if (self.markMonth() >= Number(3751.06) && self.markMonth() < Number(4664.69)) {
				return Number(22.5);
			} else if (self.markMonth() >= Number(4664.69)) {
				return Number(27.5);
			}
		});

		///////////////////////////////////////////////////////////
		// Functions
		//
		self.init = function() {
			self.getValues();
			self.listener();
		};

		self.getValues = function() {
			$.ajax({
					url: '/v1/configuracao/getProlabore',
					type: 'GET',
				dataType: 'json',
					success: function(data) {
						self.mark(data.response.fMark);
					}
				});
		};

		self.listener = function() {
			$('[btn-prolabore-save]').on('click', function() {
				$.ajax({
					url: '/v1/configuracao/addProlabore',
					type: 'POST',
					data: {
						mark: self.mark()
					},
					success: function(data) {

					}
				});
			});
		};
	};

	var init = function() {
		window.prolabore = new prolabore;
		ko.applyBindings(window.prolabore, document.getElementById('prolabore'));
		window.prolabore.init();
	};

	var callback = function(func) {
		callback = func;
	};

	return {
		init: init,
		callback: function(func) {
			if ((typeof func !== 'function')) throw Error('invalid argument: callback is not a function');
			callback(func);
		}
	}
};