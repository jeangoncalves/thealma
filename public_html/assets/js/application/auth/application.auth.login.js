var Auth = function() {

    var init = function() {
        setListener();
    };

    var setListener = function() {
        $('[data-button-login]').on('click', function() {
            login();
        });

        $('input').on('keypress', function(e) {
            if(e.which == 13) {
                login();
            }
        });
    }

    var login = function() {
        $('#login-progress').removeClass('hidden');
        $.ajax({
            url: '/v1/login',
            type: 'POST',
            dataType: 'json',
            data: {
                login: $('#sName').val(),
                pass: $('#sPassword').val()
            },
            success: function(data) {
                if (data._meta.return == 'SUCCESS') {
                    window.location.href = '/app'
                } else {
                    errorLogin();
                    // console.log('teste');
                }
            },
        });
    };

    var errorLogin = function() {
        $('#login-progress .progress-bar-success').addClass('hidden');
        $('#login-progress .progress-bar-danger').removeClass('hidden');
        setTimeout(function() {
            $('#login-progress .progress-bar-success').removeClass('hidden');
            $('#login-progress .progress-bar-danger').addClass('hidden');
            $('#login-progress').addClass('hidden');
        },3000);
    };

    return {
        init: function() {
            init();
        }
    };
};

$(document).ready(function() {
    var auth = new Auth;
    auth.init();
});