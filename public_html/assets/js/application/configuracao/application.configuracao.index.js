// Carrega o script do Custo Fixo
var fixedCost = new FixedCost;
fixedCost.init();

// Carrega o script do Horas Trabalhadas
var workTime = new WorkTime;
workTime.init();

// Carrega o script do Prolabore
var prolabore = new Prolabore;
prolabore.init();