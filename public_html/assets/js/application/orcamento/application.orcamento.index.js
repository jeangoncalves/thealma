
var step = function(s) {
	var self = this;

	self.id   = ko.observable((s !== undefined) ? s.id : '');
	self.name = ko.observable((s !== undefined) ? s.sTitle : '');
	self.time = ko.observable((s !== undefined) ? s.iTime : '');
};

var list = function(l) {
	var self = this;

	self.id = ko.observable((l !== undefined) ? l.id : '');
	self.sTitle = ko.observable((l !== undefined) ? l.sTitle : '');
	self.iEngaged = ko.observable((l !== undefined) ? l.iEngaged : '');
	self.iUrgency = ko.observable((l !== undefined) ? l.iUrgency : '');
	self.iProfit = ko.observable((l !== undefined) ? l.iProfit : '');
	self.iTax = ko.observable((l !== undefined) ? l.iTax : '');
	self.sStatus = ko.observable((l !== undefined) ? l.sStatus : '');
	self.iSteps = ko.observable((l !== undefined) ? l.iSteps : '');
	self.iTimes = ko.observable((l !== undefined) ? l.iTimes : '');
	self.iDays = ko.computed(function() {
		return Math.ceil(self.iTimes()/24);
	});
};

var orcamentoModel = function() {
	var self = this;

	self.arrList = ko.observableArray();

	// MODAL
	self.id = ko.observable('');
	self.sTitle = ko.observable('');
	self.iReserveTime = ko.observable(0);
	self.iEngaged = ko.observable(0);
	self.iUrgency = ko.observable(0);
	self.iProfit = ko.observable(0);
	self.arrStep = ko.observableArray();
	self.configuration = ko.observable();
	self.TotalProject = ko.observable();

	self.sumHour = ko.computed(function() {
		var total = 0;
		$.each(self.arrStep(), function(k, v) {
			total += Number(v.time());
		});
		return Math.round(total);
	});

	self.totalHour = ko.computed(function() {
		var total = Number(self.sumHour()) + (Number(self.sumHour()) * Number(self.iReserveTime()) / 100);
		return Math.round(total);
	});

	self.TotalProject = ko.computed(function() {
		if (self.configuration() === undefined) {
			return 0;
		}
		var soma1 = (self.totalHour() * self.configuration().totalHour);
		var soma2 = ((self.iEngaged()/100) * self.totalHour() * self.configuration().costProlabore);
		var soma3 = ((self.iUrgency()/100) * self.configuration().totalHour);
		var soma4 = soma1 - soma2 + soma3 + ((self.iProfit()/100) * self.totalHour() * self.configuration().totalHour);
		return soma4.toFixed(2);
	});

	self.iTax = ko.computed(function() {
		if (self.configuration() === undefined) {
			return 0;
		}
		var total = self.TotalProject() * (self.configuration().tributeCost / 100);
		return total.toFixed(2);
	});

	self.TotalFinalProject = ko.computed(function() {
		if (self.configuration() === undefined) {
			return 0;
		}
		var total = Number(self.TotalProject()) + Number(self.iTax());
		return total.toFixed(2);
	});

	self.totalDiscount = ko.computed(function() {
		if (self.configuration() === undefined) {
			return 0;
		}
		var total = (self.iEngaged()/100) * self.totalHour() * self.configuration().costProlabore;
		return total.toFixed(2);
	});

	self.totalProfit = ko.computed(function() {
		if (self.configuration() === undefined) {
			return 0;
		}
		var total = (self.iProfit()/100) * self.totalHour() * self.configuration().totalHour - (self.iEngaged()/100) * self.totalHour() * self.configuration().costProlabore;
		return total.toFixed(2);
	});

	self.deadLine = ko.computed(function() {
		if (self.configuration() === undefined) {
			return 0;
		}
		var total = self.totalHour() / self.configuration().hour;
		return total.toFixed(2);
	});

	self.init = function() {
		actionListen();
		get();
		getConfiguration();
	};

	self.stepRemove = function(step) {
		self.arrStep.remove(step);
		if (self.id() !== '') {
			self.removeStep(step.id());
		}
	};

	self.add = function() {
		$.ajax({
			url: '/v1/orcamento/add',
			type: 'POST',
			dataType: 'json',
			data: {
				id: self.id,
				sTitle: self.sTitle(),
				iReserveTime: self.iReserveTime(),
				iEngaged: self.iEngaged(),
				iUrgency: self.iUrgency(),
				iProfit: self.iProfit(),
				iTax: self.iTax(),
				steps: self.arrStep()
			},
			success: function(data) {
				$('#add-orcamento').modal('hide');
				get();
			},
			error: function(data) {
				console.warn('error', data);
			}
		});
	};

	self.removeStep = function(id) {

		jQuery.ajax({
			url: '/v1/orcamento/removeStep',
			method: 'POST',
			dataType: 'json',
			data: {
				id:  id 
			},
			success: function(data) {
				if (data.response.return) {
					get();
				}
			},
			error: function(data) {
				console.warn(data);
			}
		});
	};

	var getConfiguration = function() {
		$.ajax({
			url: '/v1/configuracao/getAllConfiguration',
			type: 'GET',
			dataType: 'json',
			success: function(data) {
				self.configuration(data.response);
			}
		});
	};

	var actionListen = function() {

		$('[data-button-step]').click(function() {
			self.arrStep.push(new step());
		});
		$('[data-button-save]').click(function() {
			self.add();
		});
		$('#add-orcamento').on('hidden.bs.modal', function () {
			cleanModal();
		});
	};

	var afterGet = function() {
		$('[data-table]').on('click',function() {
			modalEdit($(this).data('value'));
		});
	};

	var get = function() {
		self.arrList.removeAll();
		$.ajax({
			url: '/v1/orcamento',
			dataType: 'json',
			success: function(data) {
				$.each(data.response, function(k, v) {
					self.arrList.push(new list(v));
				});
			},
			complete: function(data) {
				afterGet();
			}
		});
	};

	var modalEdit = function(id) {
		$('#add-orcamento').modal('show');
		$.ajax({
			url: '/v1/orcamento/' + id,
			type: 'GET',
			dataType: 'json',
			success: function(data) {
				self.id(data.response.id);
				self.sTitle(data.response.sTitle);
				self.iReserveTime(data.response.iReserveTime);
				self.iEngaged(data.response.iEngaged);
				self.iUrgency(data.response.iUrgency);
				self.iProfit(data.response.iProfit);
				if (data.response.steps !== undefined) {
					$.each(data.response.steps, function(k, v) {
						self.arrStep.push(new step(v));
					});
				}
			}
		});
	};

	var cleanModal = function() {
		self.id('');
		self.sTitle('');
		self.iReserveTime(0);
		self.iEngaged(0);
		self.iUrgency(0);
		self.iProfit(0);
		self.arrStep.removeAll();
	};
};

window.orcamentoModel = new orcamentoModel();
ko.applyBindings(window.orcamentoModel, document.getElementById('container-orcamento'));
orcamentoModel.init();
