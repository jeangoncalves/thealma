<!DOCTYPE html>
<html lang="pt">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>Contate-nos</title>
	<base href="{{base_url}}" />
			<meta name="viewport" content="width=992" />
		<meta name="description" content="" />
	<meta name="keywords" content="" />
		<meta name="generator" content="Zyro - Website Builder" />
	
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
	<script src="js/main.js" type="text/javascript"></script>

	<link href="css/site.css?v=1.1.21" rel="stylesheet" type="text/css" />
	<link href="css/common.css?ts=1454860159" rel="stylesheet" type="text/css" />
	<link href="css/3.css?ts=1454860159" rel="stylesheet" type="text/css" />
	
	<script type="text/javascript">var currLang = '';</script>		
	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>


<body>{{ga_code}}<div class="root"><div class="vbox wb_container" id="wb_header">
	
<div id="wb_element_instance43" class="wb_element"><ul class="hmenu"><li><a href="P%C3%A1gina-Inicial/" target="_self" title="Página Inicial">Página Inicial</a></li><li><a href="Produtos/" target="_self" title="Produtos">Produtos</a></li><li class="active"><a href="Contate-nos/" target="_self" title="Contate-nos">Contate-nos</a></li></ul></div><div id="wb_element_instance44" class="wb_element" style=" line-height: normal;"><h4 class="wb-stl-pagetitle">Software</h4></div><div id="wb_element_instance45" class="wb_element"><img alt="" src="gallery_gen/4ef815bff3228807fbb87f7fc5d30562_50x60.png"></div></div>
<div class="vbox wb_container" id="wb_main">
	
<div id="wb_element_instance48" class="wb_element" style=" line-height: normal;"><h1 class="wb-stl-heading1">Contate-nos</h1></div><div id="wb_element_instance49" class="wb_element"><form class="wb_form" method="post"><input type="hidden" name="wb_form_id" value="844a448c"><textarea name="message" rows="3" cols="20" class="hpc"></textarea><table><tr><th>Nome&nbsp;&nbsp;</th><td><input type="hidden" name="wb_input_0" value="Nome"><input class="form-control form-field" type="text" value="" name="wb_input_0"></td></tr><tr><th>E-mail&nbsp;&nbsp;</th><td><input type="hidden" name="wb_input_1" value="E-mail"><input class="form-control form-field" type="text" value="" name="wb_input_1"></td></tr><tr class="area-row"><th>Mensagem&nbsp;&nbsp;</th><td><input type="hidden" name="wb_input_2" value="Mensagem"><textarea class="form-control form-field form-area-field" rows="3" cols="20" name="wb_input_2"></textarea></td></tr><tr class="form-footer"><td colspan="2"><button type="submit" class="btn btn-default">Enviar</button></td></tr></table></form><script type="text/javascript">
			var formValues = <?php echo json_encode($_POST); ?>;
			var formErrors = <?php global $formErrors; echo json_encode($formErrors); ?>;
			wb_form_validateForm("844a448c", formValues, formErrors);
			<?php global $wb_form_send_state; if (isset($wb_form_send_state) && $wb_form_send_state) { ?>
				setTimeout(function() {
					alert("<?php echo addcslashes($wb_form_send_state, "\\'\"&\n\r\0\t<>"); ?>");
				}, 1);
			<?php } ?>	
			</script></div><div id="wb_element_instance50" class="wb_element" style=" line-height: normal;"><p class="wb-stl-normal">+00 000 00000</p>
<p class="wb-stl-normal">Edifício Empire State</p>
<p class="wb-stl-normal">350 5th Ave</p>
<p class="wb-stl-normal">Nova York</p>
<p class="wb-stl-normal">NY 10118</p>
<p class="wb-stl-normal">EUA</p></div><div id="wb_element_instance51" class="wb_element" style=" line-height: normal;"><p class="wb-stl-normal">Texto curto 3 para template Texto curto 3 para template Texto curto 3 para template Texto curto 3 para template Texto curto 3 para template Texto curto 3 para...</p></div><div id="wb_element_instance52" class="wb_element"><script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false&amp;libraries=places"></script><script type="text/javascript">
				function initialize() {
					if (window.google) {
						var div = document.getElementById("wb_element_instance52");
						var ll = new google.maps.LatLng(40.689247,-74.044502);
						var map = new google.maps.Map(div, {
							zoom: 16,
							center: ll,
							mapTypeId: "roadmap"
						});
						
						var marker = new google.maps.Marker({
							position: ll,
							clickable: false,
							map: map
						});
					}
				}
				google.maps.event.addDomListener(window, "load", initialize);
			</script></div><div id="wb_element_instance53" class="wb_element" style="width: 100%;">
			<?php
				global $show_comments;
				if (isset($show_comments) && $show_comments) {
					renderComments(3);
			?>
			<script type="text/javascript">
				$(function() {
					var block = $("#wb_element_instance53");
					var comments = block.children(".wb_comments").eq(0);
					var contentBlock = $("#wb_main");
					contentBlock.height(contentBlock.height() + comments.height());
				});
			</script>
			<?php
				} else {
			?>
			<script type="text/javascript">
				$(function() {
					$("#wb_element_instance53").hide();
				});
			</script>
			<?php
				}
			?>
			</div></div>
<div class="vbox wb_container" id="wb_footer" style="height: 124px;">
	
<div id="wb_element_instance46" class="wb_element" style=" line-height: normal;"><p class="wb-stl-footer">© 2016 <a href="http://thealma.esy.es">thealma.esy.es</a></p></div><div id="wb_element_instance47" class="wb_element"><div class="wb-stl-footer" style="white-space: nowrap;">Membro de <i class="icon-wb-logo"></i><a href="http://zyro.com/examples/" target="_blank" title="Zyro - Website Builder">Zyro</a></div><script type="text/javascript">
				var _siteProBadge = _siteProBadge || [];
				_siteProBadge.push({hash: "b93b451d63db239239152c92f6c989b5", cont: "wb_element_instance47"});

				(function() {
					var script = document.createElement("script");
					var src = "http://zyro.com/examples/getjs/";
					script.type = "text/javascript";
					script.async = true;
					script.src = src.replace(/http.*:/, location.protocol);
					var s = document.getElementsByTagName("script")[0];
					s.parentNode.insertBefore(script, s);
				})();
				</script></div><div id="wb_element_instance54" class="wb_element" style="text-align: center; width: 100%;"><div class="wb_footer"></div><script type="text/javascript">
			$(function() {
				var footer = $(".wb_footer");
				var html = (footer.html() + "").replace(/^\s+|\s+$/g, "");
				if (!html) {
					footer.parent().remove();
					footer = $("#wb_footer");
					footer.height(70);
				}
			});
			</script></div></div><div class="wb_sbg"></div></div></body>
</html>
