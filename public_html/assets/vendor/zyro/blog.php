<!DOCTYPE html>
<html lang="pt">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>Blog</title>
	<base href="{{base_url}}" />
			<meta name="viewport" content="width=992" />
		<meta name="description" content="" />
	<meta name="keywords" content="Blog" />
		<meta name="generator" content="Zyro - Website Builder" />
	
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
	<script src="js/main.js" type="text/javascript"></script>

	<link href="css/site.css?v=1.1.21" rel="stylesheet" type="text/css" />
	<link href="css/common.css?ts=1454860159" rel="stylesheet" type="text/css" />
	<link href="css/blog.css?ts=1454860159" rel="stylesheet" type="text/css" />
	
	<script type="text/javascript">var currLang = '';</script>		
	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>


<body>{{ga_code}}<div class="root"><div class="vbox wb_container" id="wb_main" style="height: 190px;">
	
<div id="wb_element_instance62" class="wb_element"><ul class="hmenu"><li><a href="P%C3%A1gina-Inicial/" target="_self" title="Página Inicial">Página Inicial</a></li><li><a href="Produtos/" target="_self" title="Produtos">Produtos</a></li><li><a href="Contate-nos/" target="_self" title="Contate-nos">Contate-nos</a></li></ul></div><div id="wb_element_instance63" class="wb_element" style=" line-height: normal;"><h4 class="wb-stl-pagetitle">Software</h4></div><div id="wb_element_instance64" class="wb_element"><img alt="" src="gallery_gen/4ef815bff3228807fbb87f7fc5d30562_50x60.png"></div><div id="wb_element_instance65" class="wb_element" style=" line-height: normal;"><p class="wb-stl-footer">© 2016 <a href="http://thealma.esy.es">thealma.esy.es</a></p></div><div id="wb_element_instance66" class="wb_element"><div class="wb-stl-footer" style="white-space: nowrap;">Membro de <i class="icon-wb-logo"></i><a href="http://zyro.com/examples/" target="_blank" title="Zyro - Website Builder">Zyro</a></div><script type="text/javascript">
				var _siteProBadge = _siteProBadge || [];
				_siteProBadge.push({hash: "b93b451d63db239239152c92f6c989b5", cont: "wb_element_instance66"});

				(function() {
					var script = document.createElement("script");
					var src = "http://zyro.com/examples/getjs/";
					script.type = "text/javascript";
					script.async = true;
					script.src = src.replace(/http.*:/, location.protocol);
					var s = document.getElementsByTagName("script")[0];
					s.parentNode.insertBefore(script, s);
				})();
				</script></div><div id="wb_element_instance67" class="wb_element" style="width: 100%;">
			<?php
				global $show_comments;
				if (isset($show_comments) && $show_comments) {
					renderComments(blog);
			?>
			<script type="text/javascript">
				$(function() {
					var block = $("#wb_element_instance67");
					var comments = block.children(".wb_comments").eq(0);
					var contentBlock = $("#wb_main");
					contentBlock.height(contentBlock.height() + comments.height());
				});
			</script>
			<?php
				} else {
			?>
			<script type="text/javascript">
				$(function() {
					$("#wb_element_instance67").hide();
				});
			</script>
			<?php
				}
			?>
			</div><div id="wb_element_instance68" class="wb_element" style="text-align: center; width: 100%;"><div class="wb_footer"></div><script type="text/javascript">
			$(function() {
				var footer = $(".wb_footer");
				var html = (footer.html() + "").replace(/^\s+|\s+$/g, "");
				if (!html) {
					footer.parent().remove();
					footer = $("#wb_footer");
					footer.height(0);
				}
			});
			</script></div></div><div class="wb_sbg"></div></div></body>
</html>
