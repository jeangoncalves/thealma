<?php

namespace app\DataAccess;

use PDO;

class UserDataAccess extends \Piano\Mvc\DataAccessAbstract
{
    protected $table;
    protected $model;
    protected $ini 	 = '../src/app/config/config.ini';
    protected $applicationEnv = 'production';
    protected $conf;

    public function __construct($model = null)
    {
        if ($model !== null) {
            $this->table = strtolower($model);
            $this->model = 'app\dataAccess\models\\'.ucfirst($model);
        }
        $this->setupEnv();        
        $this->pdo = new PDO($this->conf['dbAdapter'].":host=".$this->conf['dbHost'].";dbname=".$this->conf['dbName'].";charset=utf8;", $this->conf['dbUser'], $this->conf['dbPass']);
    }

    private function setupEnv()
    {
        if (getenv('APPLICATION_ENV') != '') {
            $this->applicationEnv = getenv('APPLICATION_ENV');
        }

        $config = new \Piano\Config\Ini($this->ini);
        $this->conf = $config->get($this->applicationEnv);
    }
}
