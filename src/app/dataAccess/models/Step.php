<?php

namespace app\dataAccess\models;

class Step
{
    private $id;
    private $iEstimate;
    private $sTitle;
    private $iTime;

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setEstimate($estimate)
    {
        $this->iEstimate = $estimate;
    }

    public function getEstimate()
    {
        return $this->iEstimate;
    }

    public function setTitle($title)
    {
        $this->sTitle = $title;
    }

    public function getTitle()
    {
        return $this->sTitle;
    }

    public function setTime($time)
    {
        $this->iTime = $time;
    }

    public function getTime()
    {
        return $this->iTime;
    }
}