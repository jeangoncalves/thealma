<?php

namespace app\dataAccess\models;

class Estimate
{
    private $id;
    private $iUser;
    private $sTitle;
    private $iReserveTime;
    private $iEngaged;
    private $iUrgency;
    private $iProfit;
    private $sStatus;

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setUser($user)
    {
        $this->iUser = $user;
    }

    public function getUser()
    {
        return $this->iUser;
    }

    public function setTitle($title)
    {
        $this->sTitle = $title;
    }

    public function getTitle()
    {
        return $this->sTitle;
    }

    public function setReserveTime($iReserveTime)
    {
        $this->iReserveTime = $iReserveTime;
    }

    public function getReserveTime()
    {
        return $this->iReserveTime;
    }

    public function setEngaged($engaged)
    {
        $this->iEngaged = $engaged;
    }

    public function getEngaged()
    {
        return $this->iEngaged;
    }

    public function setUrgency($urgency)
    {
        $this->iUrgency = $urgency;
    }

    public function getUrgency()
    {
        return $this->iUrgency;
    }

    public function setProfit($profit)
    {
        $this->iProfit = $profit;
    }

    public function getProfit()
    {
        return $this->iProfit;
    }

    public function setStatus($status)
    {
        $this->sStatus = $status;
    }

    public function getStatus()
    {
        return $this->sStatus;
    }
}