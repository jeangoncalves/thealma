<?php

namespace app\dataAccess\models;

class Cost
{
    private $id;
    private $iUser;
    private $sName;
    private $eType;
    private $fValue;

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setUser($iUser)
    {
        $this->iUser = $iUser;
    }

    public function getUser()
    {
        return $this->iUser;
    }

    public function setName($sName)
    {
        $this->sName = $sName;
    }

    public function getName()
    {
        return $this->sName;
    }

    public function setType($eType)
    {
        $this->eType = $eType;
    }

    public function getType()
    {
        return $this->eType;
    }

    public function setValue($fValue)
    {
        $this->fValue = $fValue;
    }

    public function getValue()
    {
        return $this->fValue;
    }
}