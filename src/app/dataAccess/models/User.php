<?php

namespace app\dataAccess\models;

class User
{
    private $id;
    private $name;
    private $email;
    private $password;
    private $token;
    private $dtCreated;
    private $status;

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function setToken($token)
    {
        $this->token = $token;
    }

    public function setDtCreated($dtCreated)
    {
        $this->dtCreated = $dtCreated;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function getDtCreated()
    {
        return $this->dtCreated;
    }

    public function getStatus()
    {
        return $this->status;
    }
}