<?php

namespace app\dataAccess\models;

class Configuration
{
    private $id;
    private $iUser;
    private $iDays;
    private $fHours;
    private $fMark;

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setUser($iUser)
    {
        $this->iUser = $iUser;
    }

    public function getUser()
    {
        return $this->iUser;
    }

    public function setDays($iDays)
    {
        $this->iDays = $iDays;
    }

    public function getDays()
    {
        return $this->iDays;
    }

    public function setHours($ifHours)
    {
        $this->fHours = $fHours;
    }

    public function getHours()
    {
        return $this->fHours;
    }

    public function setMark($fMark)
    {
        $this->fMark = $fMark;
    }

    public function getMark()
    {
        return $this->fMark;
    }
}