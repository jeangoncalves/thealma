<?php

namespace app\modules\application\controllers;

use Piano\Mvc\Controller;
use PDO;

class ConfiguracaoController extends Controller
{
    public function indexAction()
    {
        $params = [
            'title'     => 'The ALMA',
            'self'      => $this
        ];

        $this->view->setJs([
            '/assets/js/src/configuracao/fixedCost.js',
            '/assets/js/src/configuracao/workTime.js',
            '/assets/js/src/configuracao/prolabore.js',
        ]);
        $this->view->render('index', $params);
        $this->view->loadJs();
    }

    public function horasTrabalhadasAction()
    {
        $params = [
            'title'     => 'The ALMA'
        ];

        $this->view->render('horasTrabalhadas', $params);
    }

    public function custosFixosAction()
    {
        $params = [
            'title'     => 'The ALMA'
        ];

        $this->view->render('custosFixos', $params);
    }

    public function prolaboreAction()
    {
        $params = [
            'title'     => 'The ALMA'
        ];

        $this->view->render('prolabore', $params);
    }

    public function planosAction()
    {
    	$params = [
    		'title'		=> 'The ALMA'
    	];

        $this->view->render('planos', $params);
    }
}