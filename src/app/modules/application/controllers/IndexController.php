<?php

namespace app\modules\application\controllers;

use Piano\Mvc\Controller;
use PDO;

class IndexController extends Controller
{
    public function indexAction()
    {    	
        $model = new \app\dataAccess\UserDataAccess('user');
    	$params = [
    		'title'		=> 'The ALMA'
    	];

        $this->view->render('index', $params);
    }
}