<?php

namespace app\modules\application\controllers;

use Piano\Mvc\Controller;
use PDO;

class AuthController extends Controller
{
    public function loginAction()
    {
    	$params = [
    		'title'		=> 'The ALMA'
    	];

        $this->view->render('login', $params);
    }

    public function logoutAction()
    {
    	$this->destroySession();;
    	$app = $this->getApplication();
    	$app->redirect('/application/auth/login');
    }
}