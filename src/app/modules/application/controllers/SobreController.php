<?php

namespace app\modules\application\controllers;

use Piano\Mvc\Controller;
use PDO;

class SobreController extends Controller
{
    public function indexAction()
    {
    	$params = [
    		'title'		=> 'The ALMA'
    	];

        $this->view->render('index', $params);
    }
}