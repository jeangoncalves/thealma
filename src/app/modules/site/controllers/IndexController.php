<?php

namespace app\modules\site\controllers;

use Piano\Mvc\Controller;
use PDO;

class IndexController extends Controller
{
	private $dirname = 'assets/vendor/zyro/';
	private $globalCSS = [ 'css/bootstrap.min.css', 'css/common.css','css/site.css'];

	public function indexAction()
	{
		$this
			->globalCSS()
			->addCss( $this->dirname . 'css/1.css');

		$params = [
			'action' => 'index',
			'title' => 'Página Inicial',
			'dirname' => $this->dirname
		];
		$this->view->render('index', $params);
	}

	public function produtosAction()
	{
		$this
			->globalCSS()
			->addCss( $this->dirname . 'css/2.css');

		$params = [
			'action' => 'produtos',
			'title' => 'Produtos',
			'dirname' => $this->dirname
		];
		$this->view->render('produtos', $params);
	}

	public function contatoAction()
	{
		$this
			->globalCSS()
			->addCss( $this->dirname . 'css/3.css');

		$params = [
			'action' => 'contato',
			'title' => 'Contato',
			'dirname' => $this->dirname
		];
		$this->view->render('contato', $params);
	}

	/**
	* funcao para adicionar o css generico
	* @return View
	*/
	private function globalCSS(){
		foreach ($this->globalCSS as $c => $ss) {
			$this->view->addCss( $this->dirname . $ss );
		}
		return $this->view;
	}
}