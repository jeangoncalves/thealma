<?php

namespace app\modules\api\controllers;

use Piano\Mvc\Controller;
use PDO;

class AuthController extends Controller
{
    public function loginAction()
    {
        $model = new \app\dataAccess\UserDataAccess('user');
        $user = $model->getFirst(array(
						        	'columns' => '*',
						        	'condition' => 'email = :email and password = :password',
						        	'values' => array(
						        		array(':email', $_POST['login'], PDO::PARAM_STR),
						        		array(':password', $_POST['pass'], PDO::PARAM_STR)
						        		)
						        	));
        if ($user != null) {
        	$this->setLogin($user);
        } else {
        	echo json_encode($this->responseMessage('ERROR', null, 'Login ou senha incorreto.'));
        }
    }

    private function setLogin($user)
    {
        $this->setSession('user-name', $user->getName());
        $this->setSession('user-id', $user->getId());
        $this->setSession('user-email', $user->getEmail());
        $this->setSession('user-status', $user->getStatus());
        echo json_encode($this->responseMessage('SUCCESS'));
    }
}