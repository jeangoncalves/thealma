<?php

namespace app\modules\api\controllers;

use Piano\Mvc\Controller;
use PDO;

class ConfiguracaoController extends Controller
{
    public function getWorkTimeAction()
    {
        $model = new \app\dataAccess\UserDataAccess('configuration');
        $workTime = $model->getFirst(
                                    array(
                                       'fetchClass' => false,
                                       'columns' => '*',
                                       'condition' => 'iUser = :iUser',
                                       'values' => array(
                                           array(':iUser', $this->getSession('user-id'), PDO::PARAM_INT)
                                       )
                                    )
                                );
        echo json_encode($this->responseMessage('SUCCESS', $workTime));
    }

    public function addWorkTimeAction()
    {
        $post = $_POST;

        $model = new \app\dataAccess\UserDataAccess('configuration');
        $id = $model->update(
                            array(
                                'iDays'     => ':iDays',
                                'fHours'    => ':fHours',
                            ),
                            'iUser = :iUser',
                            array(
                                array(':iUser', $this->getSession('user-id'), PDO::PARAM_INT),
                                array(':iDays', (isset($post['days'])) ? $post['days'] : '', PDO::PARAM_INT),
                                array(':fHours', (isset($post['hours'])) ? $post['hours'] : '', PDO::PARAM_STR),
                                )
            );

        echo json_encode($this->responseMessage('SUCCESS', $id));
    }

    public function getProlaboreAction()
    {
        $model = new \app\dataAccess\UserDataAccess('configuration');
        $prolabore = $model->getFirst(
                                    array(
                                       'fetchClass' => false,
                                       'columns' => '*',
                                       'condition' => 'iUser = :iUser',
                                       'values' => array(
                                           array(':iUser', $this->getSession('user-id'), PDO::PARAM_INT)
                                       )
                                    )
                                );
        echo json_encode($this->responseMessage('SUCCESS', $prolabore));
    }
    
    public function addProlaboreAction()
    {
        $post = $_POST;

        $model = new \app\dataAccess\UserDataAccess('configuration');
        $id = $model->update(
                            array(
                                'fMark'     => ':fMark',
                            ),
                            'iUser = :iUser',
                            array(
                                array(':iUser', $this->getSession('user-id'), PDO::PARAM_INT),
                                array(':fMark', (isset($post['mark'])) ? $post['mark'] : '', PDO::PARAM_STR),
                                )
            );
        echo json_encode($this->responseMessage('SUCCESS', $id));
    }

    public function getFixedCostAction()
    {
        $model = new \app\dataAccess\UserDataAccess('cost');
        $fixedCost = $model->getAll(
                                array(
                                   'fetchClass' => false,
                                   'columns' => '*',
                                   'condition' => 'iUser = :iUser',
                                   'values' => array(
                                       array(':iUser', $this->getSession('user-id'), PDO::PARAM_INT),
                                   )
                                ),
                                'id ASC'
                            );
        echo json_encode($this->responseMessage('SUCCESS', $fixedCost));
    }

    public function addFixedCostAction()
    {
        $post = $_POST;

        $values = $post['values'];
        $response = [];
        $columns = [
           'iUser' => ':iUser',
           'sName' => ':sName',
           'eType' => ':eType',
           'fValue' => ':fValue',
        ];
        $condition = 'id = :id';

        $model = new \app\dataAccess\UserDataAccess('cost');
        foreach ($values as $value) {
            $arr = [
                    array(':iUser', $this->getSession('user-id'), PDO::PARAM_INT),
                    array(':sName', $value['name'], PDO::PARAM_STR),
                    array(':eType', $value['type'], PDO::PARAM_STR),
                    array(':fValue', $value['value'], PDO::PARAM_STR),
                ];

            if (empty($value['id'])) {
                $id = $model->insert($columns, $arr);
                array_push($response, $id);
            } else {
                array_push($arr, array(':id', $value['id'], PDO::PARAM_INT));
                $id = $model->update($columns, $condition, $arr);
                array_push($response, $id);
            }
        }

        echo json_encode($this->responseMessage('SUCCESS', $response));
    }

    public function removeFixedCostAction()
    {
        $post = $_POST;

        $model = new \app\dataAccess\UserDataAccess('cost');

        $response = $model->delete(
                                'id = :id and iUser = :iUser',
                                array(
                                   array(':id', $post['id'], PDO::PARAM_INT),
                                   array(':iUser', $this->getSession('user-id'), PDO::PARAM_INT),
                               )
                            );
        echo json_encode($this->responseMessage('SUCCESS', $response));
    }

    public function getAllConfigurationAction()
    {
        $model = new \app\dataAccess\UserDataAccess();

        $response =  $model->prepareQuery('SELECT `costProduction`,
                                                  `costProlabore`,
                                                  `hours`,
                                                  `hour`,
                                                  `markMonth`,
                                                  `markYear`,
                                                  `totalHour`,
                                                  `tributeCost`
                                            FROM vAllConfiguration
                                            WHERE iUser = '. $this->getSession('user-id'));

        echo json_encode($this->responseMessage('SUCCESS', $response[0]));
    }
}