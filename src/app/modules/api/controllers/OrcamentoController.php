<?php

namespace app\modules\api\controllers;

use Piano\Mvc\Controller;
use PDO;

class OrcamentoController extends Controller
{
    public function indexAction()
    {
        $model = new \app\dataAccess\UserDataAccess('estimate');
        $estimates =  $model->prepareQuery('SELECT e.*,
                                                   count(s.id) as iSteps,
                                                   sum(s.iTime) as iTimes
                                            FROM estimate AS e 
                                            LEFT JOIN step AS s ON s.iEstimate = e.id
                                            WHERE e.sStatus = "active"
                                            AND e.iUser = '.$this->getSession('user-id').'
                                            GROUP BY e.id');

        echo json_encode($this->responseMessage('SUCCESS', $estimates));
    }

    public function getIdAction()
    {
        $id = $this->getParam('id');    
        $model = new \app\dataAccess\UserDataAccess('estimate');
        $estimate = $model->getFirst(
                                array(
                                        'fetchClass' => false,
                                        'columns' => '*',
                                        'condition' => 'id = :id',
                                        'values' => array(
                                                    array(':id', $id, PDO::PARAM_INT)
                                                )
                                    )
                            );
        $model = new \app\dataAccess\UserDataAccess('step');
        $estimate = $estimate;
        $step = $model->getAll(
                            array(
                                    'fetchClass' => false,
                                    'columns' => '*',
                                    'condition' => 'iEstimate = :iEstimate',
                                    'values' => array(
                                                array(':iEstimate', $estimate['id'], PDO::PARAM_INT)
                                        )
                                )
                        );
        foreach ($step as $value) {
            $estimate['steps'][] = $value;
        }

        echo json_encode($this->responseMessage('SUCCESS', $estimate));
    }

    public function addAction()
    {
        $post = $_POST;

        $response = '';
        if (isset($post['id']) && $post['id'] !== '') {
            $response = $this->editEstimate($post);
        } else {
            $response = $this->addEstimate($post);
        }
        $model = new \app\dataAccess\UserDataAccess('step');
        if (isset($post['steps']) && $post['steps'] !== '') {
            foreach ($post['steps'] as $value) {
                $model->insert(
                            array(
                                'iEstimate'     => ':iEstimate',
                                'sTitle'        => ':sTitle',
                                'iTime'         => ':iTime',
                                ), array(
                                        array(':iEstimate', $response, PDO::PARAM_INT),
                                        array(':sTitle', $value['name'], PDO::PARAM_STR),
                                        array(':iTime', $value['time'], PDO::PARAM_INT),
                                    ));
            }
        }
        echo json_encode($this->responseMessage('SUCCESS', $response));
    }

    private function editEstimate($post) {
        $model = new \app\dataAccess\UserDataAccess('estimate');
        $id = $model->update(
                        array(
                            'iUser'         => ':iUser',
                            'sTitle'        => ':sTitle',
                            'iReserveTime'  => ':iReserveTime',
                            'iEngaged'      => ':iEngaged',
                            'iUrgency'      => ':iUrgency',
                            'iProfit'       => ':iProfit',
                        ),
                        'id = :id',
                        array(
                            array(':id',            $post['id'], PDO::PARAM_INT),
                            array(':iUser',         (isset($post['iUser'])) ? $post['iUser'] : '', PDO::PARAM_INT),
                            array(':sTitle',        (isset($post['sTitle'])) ? $post['sTitle'] : '', PDO::PARAM_STR),
                            array(':iReserveTime',  (isset($post['iReserveTime'])) ? $post['iReserveTime'] : '', PDO::PARAM_INT),
                            array(':iEngaged',      (isset($post['iEngaged'])) ? $post['iEngaged'] : '', PDO::PARAM_INT),
                            array(':iUrgency',      (isset($post['iUrgency'])) ? $post['iUrgency'] : '', PDO::PARAM_INT),
                            array(':iProfit',       (isset($post['iProfit'])) ? $post['iProfit'] : '', PDO::PARAM_INT)
                       )
                    );
        return $id;
    }

    private function addEstimate($post) {
        $model = new \app\dataAccess\UserDataAccess('estimate');
        $id = $model->insert(
                        array(
                            'iUser'         => ':iUser',
                            'sTitle'        => ':sTitle',
                            'iReserveTime'  => ':iReserveTime',
                            'iEngaged'      => ':iEngaged',
                            'iUrgency'      => ':iUrgency',
                            'iProfit'       => ':iProfit',
                        ), array(
                                array(':iUser', $this->getSession('user-id'), PDO::PARAM_INT),
                                array(':sTitle', $post['sTitle'], PDO::PARAM_STR),
                                array(':iReserveTime', $post['iReserveTime'], PDO::PARAM_INT),
                                array(':iEngaged', $post['iEngaged'], PDO::PARAM_INT),
                                array(':iUrgency', $post['iUrgency'], PDO::PARAM_INT),
                                array(':iProfit', $post['iProfit'], PDO::PARAM_INT)
                            )
                    );
        return $id;
    }

    public function removeStepAction()
    {
        if (!isset($_POST)) {
            throw new Exception("Error Processing Request: Remove Step request 1 parameter", 1);
        }

        $model = new \app\dataAccess\UserDataAccess('step');
        $status = $model->delete(
                                'id = :id',
                                    array(
                                       array(':id', $_POST['id'], PDO::PARAM_INT),
                                   )
                                );

        echo json_encode($this->responseMessage('SUCCESS', array('return' => $status)));
    }
}